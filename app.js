//Selectors
const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoList = document.querySelector('.todo-list');
const filterOption = document.querySelector('.filter-todo');

//Event listener
document.addEventListener("DOMContentLoaded", getTodos);
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);
filterOption.addEventListener('click', filterTodo);

//Functions

function addTodo(e){
    e.preventDefault();

    // Todo Div 
    const todoDiv = document.createElement("div"); 
    todoDiv.classList.add("todo");
    // Create li
    const newTodo = document.createElement("li"); 
    newTodo.innerText = todoInput.value;; 
    newTodo.classList.add("todo-item");
    todoDiv.appendChild(newTodo);
    // Save to localStorage 
    saveLocalTodos(todoInput.value);
    // Check button
    const doneButton = document.createElement("button"); 
    doneButton.innerHTML = '<i class="fas fa-check"></i>';
    doneButton.classList.add("done-button"); 
    todoDiv.appendChild(doneButton);
    // Trash button
    const trashButton = document.createElement("button"); 
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add("trash-button"); 
    todoDiv.appendChild(trashButton);
    //Append to List
    todoList.appendChild(todoDiv);
    //CLEAR INPUT
    todoInput.value = "";
}

function deleteCheck(e){
    e.preventDefault(); 

    const item = e.target; 

    //Delete item
    if(item.classList[0] === "trash-button"){
        const todo = item.parentElement; 
        todo.classList.add("fall");
        removeLocalTodos(todo);
        todo.addEventListener("transitionend", function(){
            todo.remove();
        })
    }

    //Check item
    if(item.classList[0] === "done-button"){
        const todo = item.parentElement; 
        todo.classList.toggle("done");
    }
}


function filterTodo(e){
    const todos = todoList.childNodes;
    todos.forEach((todo) => {
        switch (e.target.value) {
            case "all":
                todo.style.display = "block"
                break;
            case "done": 
                if (todo.classList.contains('done')){
                    todo.style.display = "block"
                } else {
                    todo.style.display = "none";
                }
                break; 
            case "uncompleted": 
                if (!todo.classList.contains("done")){
                    todo.style.display = "block";
                } else {
                    todo.style.display = "none";
                }
                break;
        }
    });
}

function saveLocalTodos(todo) {
    //Check if there is todos
    let todos; 

    if (localStorage.getItem('todos') === null){
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }
    todos.push(todo); 
    localStorage.setItem("todos", JSON.stringify(todos));
}

function getTodos(){
     //Check if there is todos
     console.log("hey");
     let todos; 

     if (localStorage.getItem('todos') === null){
         todos = [];
     } else {
         todos = JSON.parse(localStorage.getItem('todos'));
     }
     localStorage.setItem("todos", JSON.stringify(todos));

     todos.forEach(function(todo){
         // Todo Div 
        const todoDiv = document.createElement("div"); 
        todoDiv.classList.add("todo");
        // Create li
        const newTodo = document.createElement("li"); 
        newTodo.innerText = todo;
        newTodo.classList.add("todo-item");
        todoDiv.appendChild(newTodo);
        // Check button
        const doneButton = document.createElement("button"); 
        doneButton.innerHTML = '<i class="fas fa-check"></i>';
        doneButton.classList.add("done-button"); 
        todoDiv.appendChild(doneButton);
        // Trash button
        const trashButton = document.createElement("button"); 
        trashButton.innerHTML = '<i class="fas fa-trash"></i>';
        trashButton.classList.add("trash-button"); 
        todoDiv.appendChild(trashButton);
        //Append to List
        todoList.appendChild(todoDiv);
     });
}


function removeLocalTodos(todo) {
    let todos; 

    if (localStorage.getItem('todos') === null){
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }

    const todoIndex = todo.children[0].innerText; 
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.setItem("todos", JSON.stringify(todos));
}